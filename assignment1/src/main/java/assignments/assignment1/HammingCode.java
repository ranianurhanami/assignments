package assignments.assignment1;

import java.util.ArrayList;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * Method untuk encode data.
     */
    public static String encode(String data) {
        // string untuk menyimpan data dan parity
        String newData = "";  
        // set parity mulai dari 1                                                  
        int parity = 1;                                                         
        int counter = 1;
        int ind = 0;
        while (parity < data.length() + parity + 1 && ind < data.length()) {
            if (parity == counter) {
                // menyisipkan angka 0 di posisi parity di string newData
                newData += "0";                                                 
                parity = parity * 2;            
                counter++;
            } else {
                // menyisipkan data yang sudah ada dalam string newData
                newData += data.charAt(ind);                                      
                ind++;
                counter++;
            }
        } 
        int [] fixData = new int [newData.length()];                         
        for (int i = 0; i < newData.length(); i++) {
            // melooping elemen string menjadi elemen dalam array
            fixData[i] = Integer.parseInt(String.valueOf(newData.charAt(i)));   
        } 
        for (int p = 1; p < parity; p *= 2) {                                      
            int index = p - 1;
            int sum = 0;
            while (index < newData.length()) {
                // looping data sesuai parity group
                for (int j = 0;j < p && index < newData.length();j++) { 
                    // menjumlahkan tiap data dari suatu parity         
                    sum += fixData[index];                                        
                    index++;
                } // menghitung banyak bit yang di skip
                index += p;                                                     
            } // mengubah index parity dengan angka seharusnya
            if (sum % 2 == 0) {                                                  
                fixData[p - 1] = 0;                                                 
            } else {
                fixData[p - 1] = 1;
            }
        } // String baru untuk merubah array jadi string
        String fix = "";                                                      
        for (int j = 0; j < newData.length(); j++) {             
            fix += fixData[j];
        } 
        return fix;
    }
    /**
     * Method untuk decode data.
     */

    public static String decode(String code) {
        // array untuk menyimpan data
        int [] fixData = new int [code.length()];                               
        for (int i = 0; i < code.length(); i++) {
            fixData[i] = Integer.parseInt(String.valueOf(code.charAt(i)));
        } // arrayList untuk menyimpan parity
        ArrayList<Integer> parity = new ArrayList<Integer>();                
        for (int i = 1; i < code.length(); i *= 2) {
            parity.add(i);
        } 
        int check = 0;
        for (int p : parity) {                                                   
            int index = p - 1;
            int sum = 0;
            while (index < fixData.length) {
                for (int j = 0; j < p && index < fixData.length; j++) {
                    sum += fixData[index];
                    index++;
                } 
                index += p;
            } // untuk mengetahui terdapat kesalahan di index berapa   
            if (sum % 2 == 1) {                                                      
                check += p;
            }
        } 
        if (check - 1 <= 0) {                                                     
            ;                                               // agar check tidak kurang dari 0
        } else if (fixData[check - 1] == 0) { 
            fixData[check - 1] = 1;
        } else {
            fixData[check - 1] = 0;
        } 
        for (int g: parity) {
            // set nilai di parity dengan nilai lain
            fixData[g - 1] = 2;                                                   
        } 
        String fix = "";                                   // string berisi data tanpa parity   
        // looping untuk menghapus parity                                           
        for (int z = 0; z < fixData.length; z++) {                                   
            if (fixData[z] == 2) {
                ;
            } else {
                fix += fixData[z];
            }
        } 
        return fix;
    }
    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
