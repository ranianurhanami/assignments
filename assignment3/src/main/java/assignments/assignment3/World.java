package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;
    Carrier temp;
    
    //Constructor untuk class World
    public World(){
        this.listCarrier = new ArrayList<>();
    }

    // Cek apakah nama objek sudah ada sebelumnya
    protected boolean contains(String nama) {
        for(Carrier c : listCarrier){
            if (c.getNama().equals(nama)) {
                return true;
            } 
        } return false;

    }

    // Membuat object sesuai dengan parameter yang diberikan
    public Carrier createObject(String tipe, String nama){
        if (!contains(nama)) {
            if (tipe.equalsIgnoreCase("OJOL")) {
                temp = new Ojol(nama);
                listCarrier.add(temp );
            } else if (tipe.equalsIgnoreCase("CLEANING_SERVICE")) {
                temp = new CleaningService(nama);
                listCarrier.add(temp);
            } else if (tipe.equalsIgnoreCase("JURNALIS")) {
                temp = new Jurnalis(nama);
                listCarrier.add(temp);
            } else if (tipe.equalsIgnoreCase("PEKERJA_JASA")){
                temp = new PekerjaJasa(nama);
                listCarrier.add(temp);
            } else if (tipe.equalsIgnoreCase("PETUGAS_MEDIS")){
                temp = new PetugasMedis(nama);
                listCarrier.add(temp);
            } else if (tipe.equalsIgnoreCase("PINTU")){
                temp = new Pintu(nama);
                listCarrier.add(temp);
            } else if (tipe.equalsIgnoreCase("PEGANGAN_TANGGA")){
                temp = new PeganganTangga(nama);
                listCarrier.add(temp);
            } else if (tipe.equalsIgnoreCase("ANGKUTAN_UMUM")){
                temp = new AngkutanUmum(nama);
                listCarrier.add(temp);
            } else if (tipe.equalsIgnoreCase("TOMBOL_LIFT")){
                temp = new TombolLift(nama);
                listCarrier.add(temp);
            } temp.ubahStatus("Negatif");
        } 
        return temp;
    }
           
    //Mengambil object carrier dengan nama sesuai dengan parameter
    public Carrier getCarrier(String nama){
       for (Carrier c : listCarrier){
            if(c.getNama().equals(nama)){
                return c;
            }
        }
        return null;
    }
}
