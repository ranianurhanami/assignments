package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    // Constructor untuk Petugas Medis.
    public PetugasMedis(String nama){
        super(nama);
    }

    // Petugas Medis menyembuhkan manusia
    public void obati(Manusia manusia) {
        if (manusia.getStatusCovid().equals("Positif")){
            manusia.ubahStatus("Negatif");
            jumlahDisembuhkan++;
            manusia.tambahSembuh();
            manusia.getRantaiPenular().removeAll(manusia.getRantaiPenular());
        } 
    }

    // Mengembalikan jumlah manusia yang sudah disembuhkan
    public int getJumlahDisembuhkan(){
        return jumlahDisembuhkan;
    }

    @Override
    public String toString() {
        return ("PETUGAS MEDIS " + getNama());
    }
}