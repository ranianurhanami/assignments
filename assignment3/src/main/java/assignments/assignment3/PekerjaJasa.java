package assignments.assignment3;

public class PekerjaJasa extends Manusia{
      
    // Constructor untuk Pekerja Jasa
    public PekerjaJasa(String nama){
    	super(nama);
    }
      
    @Override
    public String toString() {
        return ("PEKERJA JASA " + getNama());
    }
}