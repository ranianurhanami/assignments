package assignments.assignment3;

public class AngkutanUmum extends Benda{
    
    // Mengimplementasikan abstract method yang terdapat pada class Benda.
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular()+35);
    }

    // Constructor untuk Angkutan Umum.
    public AngkutanUmum(String name){
        super(name);
    }

    @Override
    public String toString() {
        return ("ANGKUTAN UMUM " + getNama());
    }
}