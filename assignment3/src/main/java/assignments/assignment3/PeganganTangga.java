package assignments.assignment3;

public class PeganganTangga extends Benda{
    // Mengimplementasikan abstract method yang terdapat pada class Benda.
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular()+20);
    }
    // Constructor untuk Pegangan Tangga
    public PeganganTangga(String name){
        super(name);
    }
    @Override
    public String toString() {
        return String.format("PEGANGAN TANGGA " + getNama());
    }
}