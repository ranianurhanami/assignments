package assignments.assignment3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class InputOutput {

    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile;
    private String outputType;
    private World world;
    List<String> lineList = new ArrayList<String>();
    String[] word;

    // Constructor untuk InputOutput.
    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {
        this.inputFile = inputFile;
        setBufferedReader(inputType);
        this.outputFile = outputFile;
        this.outputType = outputType;
        setPrintWriter(outputType);
    }

    // Membuat BufferedReader bergantung inputType (I/O text atau input terminal)
    public void setBufferedReader(String inputType) {
        try {
            if (inputType.equalsIgnoreCase("TEXT")) {
                this.br = new BufferedReader(new FileReader(this.inputFile));
            } else if (inputType.equalsIgnoreCase("TERMINAL")){
                this.br = new BufferedReader(new InputStreamReader(System.in));
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    } 
    
    // Membuat PrintWriter bergantung inputType (I/O text atau output terminal) 
    public void setPrintWriter(String outputType){
        try {
            if (outputType.equalsIgnoreCase("TEXT")) {
                this.pw = new PrintWriter(this.outputFile);
            }
            else if (outputType.equalsIgnoreCase("TERMINAL")) {
                this.pw = new PrintWriter(System.out, true);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() throws IOException, BelumTertularException {
        world = new World();
        String res = this.br.readLine();
        setPrintWriter(this.outputType);
        try {
            while (res != null) {
                String [] word = res.split(" ");
                if (word[0].equalsIgnoreCase("ADD")) {
                    try {
                        world.createObject(word[1], word[2]);
                    } catch (Exception e) {
                        pw.println("Penambahan objek harus menggunakan format :  ADD [TIPE_CARRIER] [NAMA_OBJEK]");
                    }
                } else if(word[0].equalsIgnoreCase("POSITIFKAN")){
                    try {
                        world.getCarrier(word[1]).ubahStatus("Positif");
                    } catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    }
                } else if(word[0].equalsIgnoreCase("INTERAKSI")){ 
                    try {
                        world.getCarrier(word[1]).interaksi(world.getCarrier(word[2]));
                    }catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    }
                } else if (word[0].equalsIgnoreCase("SEMBUHKAN")) {
                    try {
                        PetugasMedis a = (PetugasMedis) world.getCarrier(word[1]);
                        Manusia b = (Manusia) world.getCarrier(word[2]);
                        a.obati(b);
                    }catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    }catch (ClassCastException ex) {
                        pw.println("Objek 1 harus bertipe Petugas Medis dan objek 2 bertipe Manusia");
                    }
                } else if (word[0].equalsIgnoreCase("BERSIHKAN")) {
                    try {
                        CleaningService a = (CleaningService) world.getCarrier(word[1]);
                        Benda b = (Benda) world.getCarrier(word[2]);
                        a.bersihkan(b);
                    } catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    } catch (ClassCastException ex) {
                        pw.println("Objek 1 harus bertipe Cleaning Service dan objek 2 bertipe Benda");
                    }
                } else if (word[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                    try {
                        PetugasMedis a = (PetugasMedis) world.getCarrier(word[1]);
                        this.pw.println(String.format("%s menyembuhkan %d manusia", a.toString(), a.getJumlahDisembuhkan()));  
                        this.pw.flush();
                    } catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    } catch (ClassCastException ex) {
                        pw.println("Objek 1 harus bertipe Petugas Medis dan objek 2 bertipe Manusia");
                    }
                } else if (word[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                    try {
                        CleaningService b = (CleaningService) world.getCarrier(word[1]);
                        this.pw.println(String.format("%s membersihkan %d benda", b.toString(), b.getJumlahDibersihkan()));  
                        this.pw.flush();
                    } catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    } catch (ClassCastException ex) {
                        pw.println("Objek 1 harus bertipe Petugas Medis dan objek 2 bertipe Manusia");
                    }
                } else if (word[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                    try {
                        this.pw.println(String.format("%s telah menyebarkan virus COVID ke %d objek", world.getCarrier(word[1]).toString(), world.getCarrier(word[1]).getTotalKasusDisebabkan())); 
                        this.pw.flush();
                    } catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    }
                } else if (word[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                    try {
                        this.pw.println((String.format("%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek", world.getCarrier(word[1]).toString(),world.getCarrier(word[1]).getAktifKasusDisebabkan()))); 
                        this.pw.flush();
                    } catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    }
                } else if(word[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")){
                    try {
                        this.pw.println(String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus", Manusia.getJumlahSembuh()  ));
                        this.pw.flush();
                    } catch (NullPointerException e) {
                        pw.println("Tidak ada data mengenai objek tersebut");
                    }
                } else if(word[0].equalsIgnoreCase("RANTAI")){
                    try {
                        if (world.getCarrier(word[1]).getStatusCovid().equals("Positif")) {
                            String a = ("Rantai Penyebaran " + world.getCarrier(word[1]).toString() + " : ");
                            List<Carrier> temp = world.getCarrier(word[1]).getRantaiPenular();
                            for (int i = 0; i < temp.size(); i++) {
                                if (i == temp.size()-1){
                                    a += temp.get(i).toString();
                                } else {
                                    a += (temp.get(i).toString() + " -> ");
                                }
                            } this.pw.println(a);
                            this.pw.flush();
                        } else {
                            String a = world.getCarrier(word[1]).toString() + " berstatus negatif";
                            throw new BelumTertularException(a);
                        }
                    } catch (BelumTertularException ex ){
                        this.pw.println(ex);
                    }
                } else if (word[0].equalsIgnoreCase("EXIT")){
                    System.exit(0);
                }
                res = this.br.readLine();
            }
        } catch (IOException e) {
            this.pw.println(e);
        }
    }

}