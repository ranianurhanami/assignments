package assignments.assignment3;

public class TombolLift extends Benda{

    // Mengimplementasikan abstract method yang terdapat pada class Benda.
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular()+20);
    }
    
    // Constructor untuk Tombol Lift.      
    public TombolLift(String name){
        super(name);
    }

    @Override
    public String toString() {
        return ("TOMBOL LIFT " + getNama());
    }
}