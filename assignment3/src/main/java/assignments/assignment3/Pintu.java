package assignments.assignment3;

public class Pintu extends Benda{
    // Mengimplementasikan abstract method yang terdapat pada class Benda.
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular()+30);
    }
    
    // Constructor untuk pintu
    public Pintu(String name){
        super(name);
    }
    
    @Override
    public String toString() {
        return ("PINTU " + getNama());
    }
}