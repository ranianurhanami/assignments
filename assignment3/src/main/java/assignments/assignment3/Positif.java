package assignments.assignment3;

public class Positif implements Status {

    public String getStatus() {
        return "Positif";
    }

    // Implementasikan apabila object Penular melakukan interaksi dengan object tertular
    public void tularkan(Carrier penular, Carrier tertular) {
        if (!(penular.getTipe().equals("Benda") && tertular.getTipe().equals("Benda"))) {
            if(tertular.getTipe().equals("Benda")){
                Benda benda = (Benda) tertular;
                benda.tambahPersentase();
                if (benda.getPersentaseMenular() >= 100) {
                    tertular.ubahStatus("Positif");
                }
            } else if(tertular.getTipe().equals("Manusia")){
                tertular.ubahStatus("Positif");
            }
        }
    }
}
    // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
