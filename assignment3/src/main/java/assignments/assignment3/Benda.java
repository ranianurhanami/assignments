package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular;

    // Constructor untuk Benda.
    public Benda(String name){
        super(name, "Benda");
    }

    public abstract void tambahPersentase();

    // Mengembalikkan nilai dari atribut persentaseMenular    
    public int getPersentaseMenular(){
        return persentaseMenular;
    }
    
    // Set persentase menular benda
    public void setPersentaseMenular(int persentase) {
        persentaseMenular = persentase;
        if(persentaseMenular >= 100){
            this.ubahStatus("Positif");
        }
    }
}