package assignments.assignment3;

public class Ojol extends Manusia{
    
    // Constructor untuk Ojol.
    public Ojol(String name){
        super(name);
    }

    @Override
    public String toString() {
        return ("OJOL " + getNama());
    }
  	
}