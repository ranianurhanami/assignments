package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    // Buat constructor untuk Manusia.
    public Manusia(String nama){
        super(nama,"Manusia");
    }
    
    // Menambahkan jumlah manusia yang sembuh
    public void tambahSembuh(){
        Manusia.jumlahSembuh++;
    }

    // Mengembalikkan jumlah manusia yang telah sembuh
    public static int getJumlahSembuh() {
        return jumlahSembuh;
    }
    
}