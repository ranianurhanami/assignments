package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    // Constructor untuk Cleaning Service.
    public CleaningService(String nama){
        super(nama);
    }

    // Cleaning service membersihkan benda
    public void bersihkan(Benda benda){
        if (benda.getStatusCovid().equals("Positif")){
            benda.setPersentaseMenular(0);
            benda.ubahStatus("Negatif"); 
            jumlahDibersihkan++;
            benda.getRantaiPenular().removeAll(benda.getRantaiPenular());
        }  else if (benda.getPersentaseMenular() < 100 && benda.getPersentaseMenular() >= 0) {
            benda.setPersentaseMenular(0);
            jumlahDibersihkan++;
            benda.getRantaiPenular().removeAll(benda.getRantaiPenular());
        }      
    }
    
    // Mengembalikkan banyak hal yang sudah dibersihkan.
    public int getJumlahDibersihkan(){
        return jumlahDibersihkan;
    }

    @Override
    public String toString() {
        return (String.format("CLEANING SERVICE %s", getNama()));
    }

}