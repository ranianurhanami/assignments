package assignments.assignment3;

public class Jurnalis extends Manusia{
    
    // Constructor untuk Jurnalis.
    public Jurnalis(String name){
        super(name);
    }

    @Override
    public String toString() {
        return ("JURNALIS " + getNama());
    }

}