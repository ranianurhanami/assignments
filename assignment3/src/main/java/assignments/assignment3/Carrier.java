package assignments.assignment3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;
    Set<Carrier> temp;
    ArrayList<Carrier> kasus;

    //Constructor untuk Carrier.
    public Carrier(String nama,String tipe){
        this.nama = nama;
        this.tipe = tipe;
        rantaiPenular = new ArrayList<>();
        kasus = new ArrayList<>();
    }

    // Kembalikan nilai dari atribut nama
    public String getNama(){
        return this.nama;
    }

    // Kembalikan nilai dari atribut tipe
    public String getTipe(){
        return this.tipe;
    }

    // Mengembalikkan status Covid seseorang.
    public String getStatusCovid(){
        return statusCovid.getStatus();
    }

    // Mengembalikkan nilai dari atribut aktifKasusDisebabkan
    public int getAktifKasusDisebabkan(){
        int i = 0;
        while (i<kasus.size()){
            if(kasus.get(i).getStatusCovid().equals("Negatif")){
                kasus.remove(kasus.get(i));
            } else i++;
        } 
        aktifKasusDisebabkan = kasus.size();
        return aktifKasusDisebabkan;
    }

    // Mengembalikkan nilai dari atribut totalKasusDisebabkan
    public int getTotalKasusDisebabkan(){
        return totalKasusDisebabkan;
    }

    // Mengembalikkan nilai dari atribut rantaiPenular
    public List<Carrier> getRantaiPenular() { 
        return rantaiPenular;
    }

    // Mengubah status covid
    public void ubahStatus(String status){
        if (status.equalsIgnoreCase("Negatif")){
            statusCovid = new Negatif();
        } else if (status.equalsIgnoreCase("Positif")) {
            statusCovid = new Positif();
        }
    }


    // Interaksi dengan objek lain
    public void interaksi(Carrier lain) {
        if (getStatusCovid().equals("Positif")) {
            this.statusCovid.tularkan(this, lain);
            if(lain.getStatusCovid().equals("Positif")) {
                if(this.rantaiPenular.size() == 0){
                    this.rantaiPenular.add(this);
                } 
                lain.rantaiPenular.addAll(this.rantaiPenular);
                lain.rantaiPenular.add(lain);
                Set<Carrier> temp = new HashSet<>(rantaiPenular);
                for (Carrier c : temp){
                    c.totalKasusDisebabkan++;
                    c.kasus.add(lain);
                }
            }
        } else if (lain.getStatusCovid().equals("Positif")){
            lain.statusCovid.tularkan(lain, this);
            if(this.getStatusCovid().equals("Positif")) {
                if(lain.rantaiPenular.size() == 0){
                    lain.rantaiPenular.add(lain);
                }
                rantaiPenular.addAll(lain.rantaiPenular);
                rantaiPenular.add(this);
                Set<Carrier> temp = new HashSet<>(lain.rantaiPenular);
                for (Carrier c : temp){
                    c.totalKasusDisebabkan++;
                    c.kasus.add(this);
                } 
            } 
        }
        
    }
    

    
    public abstract String toString();
}