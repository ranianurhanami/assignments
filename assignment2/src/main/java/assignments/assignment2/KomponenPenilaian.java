package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // Constructor untuk KomponenPenilaian.
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // Memasukkan butir ke butirPenilaian pada index ke-idx.        
        butirPenilaian[idx] = butir;
    }

    public String getNama() {
        // Mengembalikkan nama KomponenPenilaian.
        return this.nama;
    }

    public double getRerata() {
        // Mengembalikkan rata-rata butirPenilaian.
        double sum = 0.0;
        int counter = 0;
        for (ButirPenilaian i : butirPenilaian) { 
            if (i != null) {
                sum += i.getNilai(); 
                counter++;
            }
        } 
        if (counter != 0) {                
            return sum / counter;
        } 
        return 0.0;
    }
    
    public double getNilai() {
        // Mengembalikkan rerata yang sudah dikalikan dengan bobot.
        return this.getRerata() * (this.bobot / 100.00);
    }

    public String getDetail() {
        // Mengembalikkan detail KomponenPenilaian sesuai permintaan soal.
        String detail = "";
        if (butirPenilaian.length > 0) {
            detail += String.format("---------- %s ( %d %% ) ---------- \n", this.nama, this.bobot);
            if (butirPenilaian.length == 1) {
                if (butirPenilaian[0] != null) {
                    detail += (String.format("%s : %s \n", this.nama, butirPenilaian[0].toString()));
                    detail += (String.format("Kontribusi Nilai Akhir : %.2f \n", this.getNilai()));
                } else {
                    detail += (String.format("%s : 0.0 \n", this.nama));
                    detail += (String.format("Kontribusi Nilai Akhir : %.2f \n", this.getNilai()));
                }
            } else {
                for (int i = 0; i < butirPenilaian.length; i++) {
                    if (butirPenilaian[i] != null) {
                        detail += (String.format("%s %d: %s \n", this.nama,(i+1), butirPenilaian[i].toString()));
                    } 
                }
                detail += (String.format("Rerata : %.2f \n", this.getRerata()));
                detail += (String.format("Kontribusi Nilai Akhir : %.2f \n", this.getNilai()));
                } 
            } 
            return detail;
        }


    @Override
    public String toString() {
        // Mengembalikkan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        return String.format("Rerata %s: %.2f", this.nama, this.getRerata());
    }

}
