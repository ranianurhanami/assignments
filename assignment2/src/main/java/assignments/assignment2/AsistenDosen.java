package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        // Constructor untuk AsistenDosen.
        this.nama = nama;
        this.kode = kode;
        this.mahasiswa = new ArrayList<>();
    }

    public String getKode() {
        // Kembalikan kode AsistenDosen.
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // Menambahkan mahasiswa ke dalam daftar mahasiswa
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        // Mengembalikkan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        for (Mahasiswa m : mahasiswa) {
            if (m.getNpm().equals(npm)) {
                return m;
            }
        }
        return null;
    }

    public String rekap() {
        String rekap = "";
        for (Mahasiswa m : mahasiswa) {
            rekap += (m.toString() + m.rekap() + "\n");
        }
        return rekap;
    }

    public String toString() {
        return String.format("%s - %s", this.kode, this.nama);    
    }
}
