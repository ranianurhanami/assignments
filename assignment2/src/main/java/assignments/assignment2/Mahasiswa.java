package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // Membuat constructor untuk Mahasiswa.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // Mengembalikkan KomponenPenilaian yang bernama namaKomponen.
        for (KomponenPenilaian komponen : komponenPenilaian) {
            if (komponen.getNama().equals(namaKomponen)) {
                return komponen;
            }
        }
        return null;
    }
    
    public String getNpm() {
        // Mengembalikkan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // Mengembalikkan rekapan sesuai dengan permintaan soal.
        String rekapan = "\n";
        double nilaiAkhir = 0;
        for (KomponenPenilaian n : komponenPenilaian) {
            rekapan += n.toString() + "\n";
            nilaiAkhir += n.getNilai();
        } 
        rekapan += (String.format("Nilai Akhir : %.2f \n", nilaiAkhir));
        rekapan += (String.format("HURUF : %s \n",getHuruf(nilaiAkhir)));
        rekapan += getKelulusan(nilaiAkhir) + "\n";
        return rekapan;
    } 

    public String toString() {
        // Mengembalikkan representasi String dari Mahasiswa sesuai permintaan soal.
        return String.format("%s - %s", this.npm, this.nama);
    }

    public String getDetail() {
        // Mengembalikkan detail dari Mahasiswa sesuai permintaan soal.
        String detail = "";
        double nilaiAkhir = 0;
        for (KomponenPenilaian data : komponenPenilaian) {
            detail += (data.getDetail() + "\n");
            nilaiAkhir += data.getNilai();
        }
        detail += (String.format("Nilai Akhir : %.2f \n",nilaiAkhir));
        detail += (String.format("HURUF : %s \n",getHuruf(nilaiAkhir)));
        detail += getKelulusan(nilaiAkhir) + "\n";
        return detail;
    } 

    @Override
    public int compareTo(Mahasiswa other) {
        // Membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        return this.getNpm().compareTo(other.getNpm());
        }
    }