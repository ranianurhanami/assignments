package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        // Constructor untuk ButirPenilaian.
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    public double getNilai() {
        // Mengembalikkan nilai yang sudah disesuaikan dengan keterlambatan.
        if (this.nilai > 0) {
            if (this.terlambat == true) {
                return this.nilai - (this.nilai * ButirPenilaian.PENALTI_KETERLAMBATAN / 100);
            } else {
                return this.nilai;
            }
        } 
        return 0;
    }

    @Override
    public String toString() {
        // Mengembalikkan representasi String dari ButirPenilaian sesuai permintaan soal.
        if (this.terlambat == true) {
            return String.format("%.2f (T)", this.getNilai());
        } else {
            return String.format("%.2f", this.getNilai());
        }
    }
}
